#this is a comment. thank you for the help in writing this comment
ALL : code

code : code.o

% : %.o
	$(CXX) $< -o $@

%.o : %.cpp
	$(CXX) -c $< -o $@

clean:
	$(RM) code *.o
